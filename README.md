This repository is a simple 2-layer Lesnet implementation in Keras and CuDA that operates on Word Vectors rather than traditional images.

## Installation 

First you need to create a virtual environment to install python dependencies. Assuming that you have `conda` installed and configured:

```
make virtualenv
make activate
```

should create a virtual python environment with all prerequisite software (Tensorflow, Keras, Numpy) installed. `make activate` will activate the environment in the future.

## Data Prep

This repository contains the DBPedia dataset, and already has created a train and test split with a compressed Word2Vec lookup matrix. See `data`. If you would like to re-create the train and test splits (perhaps with new Word Vector Embeddings), simply run `make data`.

## Keras 

Run the Keras experiment with `make keras`

## CuDNN

Run the CuDA experiment with `make cudnn`

## Metrics

Compare the two experiments with `make compare`