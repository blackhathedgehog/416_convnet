#define TILE_WIDTH 16
#define TILE_SIZE 16

//__global__ void ConvLayerForward_Kernel(int num_input, int filter_bank_dims, 
//                     int num_filters, float *input_features, float *output)
__global__ void ConvLayerForward_Kernel(int C, int W_grid, int K, float* X, float* W, float* Y)
{
   int n, m, h0, w0, h_base, w_base, h, w;
   int X_tile_width = TILE_WIDTH + K - 1;
   extern __shared__ float shmem[];
   float* X_shared = &shmem[0]; //shared input
   float* W_shared = &shmem[X_tile_width * X_tile_width]; //shared filters
   n = blockIdx.x;
   m = blockIdx.y;
   h0 = threadIdx.x; //used as shorthand for threadIdx.x and y
   w0 = threadIdx.y;
   h_base = (blockIdx.z / W_grid) * TILE_SIZE; //vertical base out data index for the block
   w_base = (blockIdx.z % W_grid) * TILE_SIZE; //horizontal base out data index for the block
   h = h_base + h0;
   w = w_base + w0;

   float acc = 0;
   int c,i,j,p,q;

   for(c = 0; c < C; c++){
      if ((h0 < K) && (w0 < K))
         W_shared[h0, w0] = W[m,c,h0,w0]; //load weights for W[m,c..]
         __syncthreads();                 //h0 and w0 used as shorthand for threadIdx
        
      //load input tiles into shared memory 
      for(i = h; i < h_base + X_tile_width; i+= TILE_WIDTH){
         for(j = w; j < w_base + X_tile_width; j += TILE_WIDTH){
            X_shared[i - h_base, j - w_base] = X[n,c,h,w];
         }
      }  

      __syncthreads();
      for(p = 0; p < K; p++){
         for(q = 0; q < K; q++){
            acc += X_shared[h + p, w + q] * W_shared[p,q];
         }
      }
      __syncthreads();
      Y[n,m,h,w] = acc;
      
   }
}

int main(int argc, char **argv) {
   //int W_out = size of input image
   int W_out = 160;
   int H_out = 160;
   
   //output array
   float* d_output;
   float* h_output; 
   cudaMalloc(&d_output, sizeof(float) * W_out * H_out);

   //input array
   float* d_input;
   float* h_input;
   cudaMalloc(&d_input, sizeof(float) * (W_out + 10) * (H_out + 10));

   //filter
   int* d_filter;
   int h_filter[25] = {1,4,7,4,1,4,16,26,16,4,7,26,41,26,7,4,16,26,16,4,1,4,7,4,1};
   cudaMalloc(&d_filter, sizeof(int) * 25);
   cudaMemcpy(d_filter, h_filter, sizeof(int) * 25, cudaMemcpyHostToDevice);

   //other variables
   int W_grid = W_out/TILE_WIDTH;
   int H_grid = H_out/TILE_WIDTH;
   int Z = H_grid * W_grid;
   int K = 5; //height and width of filter mask
   int N = 1; //number of batches
   int M = 1; //number of output features
   int C = 1; //input feature map count
   dim3 blockDim(TILE_WIDTH, TILE_WIDTH, 1);
   dim3 gridDim(N, M, Z);
   float* shmem_size;
   cudaMalloc(&shmem_size, sizeof(float) * ((TILE_WIDTH + K - 1) * (TILE_WIDTH + K - 1) + K * K));

   ConvLayerForward_Kernel<<< gridDim, blockDim, shmem_size>>>(C, W_grid, K, d_input, d_filter, d_output);
   //int C, int W_grid, int K, float* X, float* W, float* Y
   //cudaMemcpy(d_equalized, equalized, 256 * (sizeof(unsigned int)), cudaMemcpyHostToDevice);
}
