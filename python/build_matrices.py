import gzip
import statistics

import numpy as np

from itertools import chain
from math import ceil

from gensim.corpora import Dictionary
from gensim.models import Word2Vec



def xy(src, w2v):
    with gzip.open(src, 'rt') as f:
        for l in f:
            X = [w for w in l.split(' ')[2:] if w in w2v]
            y = int(l.split('__label__')[1].split(' ')[0])
            yield X, y


def calc_words_per_line(X, z):
    counts = [len(x) for x in X]

    median = statistics.median(counts)
    stddev = statistics.stdev(counts)

    return ceil(z * stddev + median)


def prepare(w2v_src, train_src, test_src, z):
    w2v = Word2Vec.load_word2vec_format(w2v_src, binary=w2v_src.endswith('.bin'))

    X_train, y_train = zip(*xy(train_src, w2v))
    X_test, y_test = zip(*xy(test_src, w2v))
    words_per_line = calc_words_per_line(X_train, z)
    dictionary = Dictionary(chain(X_train, X_test))

    c_w2v = np.zeros((len(dictionary) + 1, w2v.syn0.shape[1]), dtype=np.float32)

    for k, w in dictionary.items():
        c_w2v[k + 1] = w2v[w]

    def padded(x):
        ret = [dictionary.token2id[w] + 1 for w in x]
        to_pad = words_per_line - min(words_per_line, len(ret))
        yield ret[:words_per_line] + [0] * to_pad

    X_train = np.fromiter((pp for x in X_train for p in padded(x) for pp in p), dtype=np.int32,
        count=len(X_train) * words_per_line).reshape((len(X_train), -1))
    X_test = np.fromiter((pp for x in X_test for p in padded(x) for pp in p), dtype=np.int32,
        count=len(X_test) * words_per_line).reshape((len(X_test), -1))

    return c_w2v, X_train, np.array(y_train, dtype=np.int8), \
        X_test, np.array(y_test, dtype=np.int8)



def main(w2v_src, train_src, test_src, z, output_fmt):
    c_w2v, X_train, y_train, X_test, y_test = prepare(w2v_src, train_src, test_src, z)

    c_w2v.tofile(output_fmt.format('c_w2v', *c_w2v.shape))
    X_train.tofile(output_fmt.format('X_train', *X_train.shape))
    y_train.tofile(output_fmt.format('y_train', 1, *y_train.shape))
    X_test.tofile(output_fmt.format('X_test', *X_test.shape))
    y_test.tofile(output_fmt.format('y_test', 1, *y_test.shape))


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('w2v_model_src')
    parser.add_argument('train_text_src')
    parser.add_argument('test_text_src')
    parser.add_argument('output_fmt')
    parser.add_argument('--z', type=float, default=3)

    args = parser.parse_args()

    main(args.w2v_model_src, args.train_text_src, args.test_text_src, 
        args.z, args.output_fmt)
