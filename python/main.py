from argparse import ArgumentParser

import numpy as np
import tensorflow as tf

from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D, Dense, Activation, Flatten
from keras.optimizers import SGD

from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report, confusion_matrix

from .utils import generate_batches, load_array

FLAGS = tf.app.flags.FLAGS
DEF = tf.app.flags

DEF.DEFINE_integer('c1_output', 32, 'first layer output filters')
DEF.DEFINE_integer('c1_filterX', 3, 'first layer filter size (X)')
DEF.DEFINE_integer('c1_filterY', 3, 'first layer filter size (Y)')

DEF.DEFINE_integer('mp1_poolX', 2, 'first layer pool (X)')
DEF.DEFINE_integer('mp1_poolY', 2, 'first layer pool (Y)')

DEF.DEFINE_integer('c2_output', 32, 'second layer output filters')
DEF.DEFINE_integer('c2_filterX', 3, 'second layer filter size (X)')
DEF.DEFINE_integer('c2_filterY', 3, 'second layer filter size (Y)')

DEF.DEFINE_integer('mp2_poolX', 2, 'second layer pool (X)')
DEF.DEFINE_integer('mp2_poolY', 2, 'second layer pool (Y)')

DEF.DEFINE_integer('batch_size', 112, 'default batch size')

DEF.DEFINE_integer('dense_neurons', 72, 'default dense neuron size (32 * 3 * 3) / (2 * 2)')


def main(c_w2v, X_train, y_train, X_test, y_test):
    input_shape = (X_train.shape[1], c_w2v.shape[1], 1)
    lb = LabelBinarizer().fit(y_train)
    nb_classes = len(lb.classes_)

    train_generator = generate_batches(c_w2v, X_train, lb.transform(y_train), FLAGS.batch_size)
    predict_generator = generate_batches(c_w2v, X_test, None, FLAGS.batch_size)

    model = build(input_shape, nb_classes)
    model.fit_generator(train_generator, samples_per_epoch=X_train.shape[0], nb_epoch=1)
    log_scores = model.predict_generator(predict_generator, len(X_test))
    evaluate(y_test, lb.inverse_transform(log_scores))


def build(input_shape, nb_classes):
    model = Sequential()
    model.add(Convolution2D(FLAGS.c1_output, FLAGS.c1_filterX, FLAGS.c1_filterY, 
        input_shape=input_shape))
    model.add(Activation('tanh'))
    model.add(MaxPooling2D(pool_size=(FLAGS.mp1_poolX, FLAGS.mp2_poolY)))
    model.add(Convolution2D(FLAGS.c2_output, FLAGS.c2_filterX, FLAGS.c2_filterY))
    model.add(Activation('tanh'))
    model.add(MaxPooling2D(pool_size=(FLAGS.mp2_poolX, FLAGS.mp2_poolY)))
    model.add(Flatten())
    model.add(Dense(FLAGS.dense_neurons))
    model.add(Activation('tanh'))
    model.add(Dense(nb_classes))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer=SGD(lr=0.01, momentum=0.0, decay=0.0, nesterov=False))
    
    return model


def evaluate(y_true, y_pred):
    print(confusion_matrix(y_true, y_pred))
    print(classification_report(y_true, y_pred))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('c_w2v_src')
    parser.add_argument('X_train_src')
    parser.add_argument('y_train_src')
    parser.add_argument('X_test_src')
    parser.add_argument('y_test_src')

    args = parser.parse_args()

    c_w2v = load_array(args.c_w2v_src, np.float32)
    X_train = load_array(args.X_train_src, np.int32)
    y_train = load_array(args.y_train_src, np.int8).flatten()
    X_test = load_array(args.X_test_src, np.int32)
    y_test = load_array(args.y_test_src, np.int8).flatten()

    main(c_w2v, X_train, y_train, X_test, y_test)
