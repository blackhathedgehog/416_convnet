import re
from itertools import cycle

import numpy as np


def load_array(src, dtype):
    p = re.compile(r'.*_(\d+)x(\d+).*')
    X = np.fromfile(src, dtype=dtype)
    return X.reshape(tuple(int(g) for g in p.match(src).groups()))


def generate_batches(w2v, X, y, batch_size):
    for start in cycle(range(0, len(X), batch_size)):
        Xb = X[start:start+batch_size]
    
        Xbt = np.array([w2v[x] for x in Xb], dtype=np.float32)
        Xbt = Xbt.reshape((batch_size, -1, len(w2v[0]), 1))

        if y is not None:
            yb = y[start:start+batch_size]
            yield Xbt, yb
        else:
            yield Xbt

        
