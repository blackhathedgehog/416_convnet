virtualenv:
	conda create -n 'convnet' -f environment.yml 

activate:
	source activate convnet

data:
	python -m python.build_matrices ~/workspace/EN.200K.cbow1_wind5_hs0_neg10_size300_smpl1e-05.txt data/dbpedia.train.gz data/dbpedia.test.gz data/{}_{}x{}.bin

keras:
	python -m python.main data/c_w2v_133977x300.bin data/X_train_560000x127.bin data/y_train_1x560000.bin data/X_test_70000x127.bin data/y_test_1x70000.bin
