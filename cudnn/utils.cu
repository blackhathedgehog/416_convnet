#include <stdlib.h>
#include <stdio.h>

#include "utils.h"

// utility function to read a binary file of doubles
void read_matrix(char *rows, char *cols, char *source, matrix_2d *ptr) {
    FILE *fp;
    int num_rows, num_cols;

    num_rows = strtol(rows, NULL, 10);
    num_cols = strtol(cols, NULL, 10);

    ptr->rows = num_rows;
    ptr->cols = num_cols;
    ptr->buffer = (double *) calloc(num_rows * num_cols, sizeof(double));

    fp = fopen(source, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);
    
    if (!fread(ptr->buffer, sizeof(double), num_rows * num_cols, fp))
        exit(EXIT_FAILURE);
}

// utility function to read a binary file of doubles
void read_int_matrix(int length, char *source, int **ptr) {
    FILE *fp;

    *ptr = (int *) calloc(length, sizeof(double));

    fp = fopen(source, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);
    
    if (!fread(*ptr, sizeof(int), length, fp))
        exit(EXIT_FAILURE);
}


// utility function to write out a binary file of doubles
void write_matrix(char *dest, matrix_2d *ptr) {
    FILE *fp;

    fp = fopen(dest, "w");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    if (!fwrite(ptr->buffer, ptr->rows * ptr->cols, sizeof(double), fp))
        exit(EXIT_FAILURE);
}
