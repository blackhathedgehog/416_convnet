typedef struct matrix_2d {
    int rows, cols;
    double *buffer;
} matrix_2d;

void read_matrix(char *rows, char *cols, char *source, matrix_2d *ptr);
void write_matrix(char *dest, matrix_2d *ptr);