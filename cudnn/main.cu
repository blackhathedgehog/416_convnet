#include <stdlib.h>
#include <cuda.h>
#include <stdio.h>

#include "utils.h"

#define BATCH_SIZE 625

#define NUM_TRAIN 560000
#define NUM_TEST 70000

#define NUM_WORDS 127
#define VECTOR_SIZE 300

#define C_W2V_SRC "data/c_w2v_133977x300.bin"
#define X_TRAIN_SRC "data/X_train_560000x127.bin"
#define Y_TRAIN_SRC "data/y_train_1x560000.bin"
#define X_TEST_SRC "data/X_test_70000x127.bin"
#define Y_TEST_SRC "data/y_test_1x560000.bin"

// TEMP 1 MULTS
#define N_MULT (BATCH_SIZE * NUM_WORDS * VECTOR_SIZE * 1)
#define C_MULT (NUM_WORDS * VECTOR_SIZE * 1)
#define HP_MULT (VECTOR_SIZE * 1)

// TEMP 2 MULTS


__global__ void ConvLayerForwardNoShare_Kernel(int C, int W_grid, int K, 
    float *X, float *W, float *Y) {

    int n, m, h, w, c, p, q;
    float acc = 0.;
    float temp1, temp2;

    n = blockId.x;
    m = blockId.y;
    h = blockId.z / W_grid + threadId.y;
    w = blockId.z % W_grid + threadId.x;

    for (c = 0; c < C; c++) { // for each channel (word)
        for (p = 0; p < K; p++) // do the KxK filter
            for (q = 0; q < K; q++) {
                // calculate input
                temp1 = X[n * N_MULT + c * C_MULT + (h + p) * HP_MULT + w + q]
                // multiply by current parameter weights W
                temp2 = W[m * N_MULT + c * C_MULT + p * HP_MULT + q]
                acc += temp1 * temp2;
            }
    }
    Y[n * N_MULT + m * N_MULT + h * HP_MULT + w] = acc;
}

int main(int argc, char *argv[]) {
    FILE *c_w2v, *X_train, *y_train, *X_test, *y_test;
    float *batch;
    int num_words;

    c_w2v = fopen(C_W2V_SRC, "r");
    if (c_w2v == NULL)
        exit(EXIT_FAILURE);
    X_train = fopen(X_TRAIN_SRC, "r");
    if (X_train == NULL)
        exit(EXIT_FAILURE);
    y_train = fopen(Y_TRAIN_SRC, "r");
    if (y_train == NULL)
        exit(EXIT_FAILURE);
    X_test = fopen(X_TEST_SRC, "r");
    if (X_test == NULL)
        exit(EXIT_FAILURE);
    y_test = fopen(Y_TEST_SRC, "r");
    if (y_test == NULL)
        exit(EXIT_FAILURE);   


}

